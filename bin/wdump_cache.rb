#!/usr/bin/env ruby
require 'fileutils'
require 'optparse'

require 'wdump_utils/wikimedia_page_doc'
require 'wdump_utils/wiktionary_entry_doc'

options = {}
OptionParser.new do |opts|
  opts.banner = "Usage: wdump_cache.rb [options]"

  opts.on('-c', '--cache CACHE_DIR', 'Path to cache directory') { |v| options[:cache] = v }
  opts.on('-d', '--dump DUMP_FILE', 'Path to Wikimedia dump file') { |v| options[:dump] = v }
  opts.on('-t', '--type DOCUMENT_TYPE', 'Document type to parse each page in the dump file as') { |v| options[:type] = v }
end.parse!

raise OptionParser::MissingArgument if options[:dump].nil?
raise OptionParser::MissingArgument if options[:type].nil?

if options[:cache]
  cache_dir = "#{options[:cache]}"
else
  cache_dir = "#{Dir.home}/.cache/wdump_utils"
end
cache_dir += "/#{options[:type]}"

FileUtils.mkdir_p cache_dir

case options[:type]
when "page"
  document = WikimediaPageDoc.new(cache_dir: cache_dir)
#when "pronunciation"
#  document = WiktionaryPronunciationDoc.new
when /wiktionary\/(.*)/
  document = WiktionaryEntryDoc.new(cache_dir: cache_dir, language: $1)
else
  puts "Unrecognized cache type"
  raise ArgumentError.new
end

parser = Nokogiri::XML::SAX::Parser.new(document)
parser.parse(File.open(options[:dump]))

#ruby wdump_cache.rb -d ~/Documents/frw/frwiktionary-20200120-pages-meta-current.xml -t page
#ruby wdump_cache.rb -d ~/Documents/frw/frwiktionary-20200120-pages-meta-current.xml -t wiktionary/fr
#ruby saxionary.rb -d entry -w ~/Documents/frw/frwiktionary-20200120-pages-meta-current.xml
