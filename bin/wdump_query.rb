#!/usr/bin/env ruby
require 'optparse'

require 'wdump_utils/wiktionary_entry'

options = {}
OptionParser.new do |opts|
  opts.banner = "Usage: wdump_query.rb [options]"

  opts.on('-c', '--cache CACHE_DIR', 'Path to cache directory') { |v| options[:cache] = v }
  opts.on('-q', '--query QUERY_ARG', 'Argument to query') { |v| options[:query] = v }
end.parse!

raise OptionParser::MissingArgument if options[:cache].nil?
raise OptionParser::MissingArgument if options[:query].nil?

cache_dir = options[:cache]
word = options[:query]

entry = WiktionaryEntry.new(cache_dir: cache_dir, word: word)

entry.sections.each do |section|
  puts "\n-----\n\n"
  puts section.name
  section.definitions.each do |definition|
    puts "\n#{definition.body}"
    definition.examples.each do |example|
      puts "- #{example}"
    end
  end
end
