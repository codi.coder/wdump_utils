require 'nokogiri'

class WikimediaPageDoc < Nokogiri::XML::SAX::Document
  @@title_tag = "title"
  @@page_tag = "page"

  def initialize(cache_dir:)
    @page_text = ""
    @handled_pages_counter = 0
    @is_within_element = {}
    @cache_dir = cache_dir
    @caching_class = self.class
  end

  def start_element name, attrs = []
    update_within_element_state name, true

    if name == @@page_tag
      @page_text = ""
    end

    if name == @@title_tag
      @page_title = ""
    end
  end

  def characters s
    if @is_within_element[@@title_tag]
      @page_title += s
    end

    if @is_within_element["text"]
      @page_text += s
    end
  end

  def end_element name
    update_within_element_state name, false

    if name == @@page_tag
      handle_page
    end
  end

  def update_within_element_state name, state
    @is_within_element[name] = state
  end

  def handle_page
    cache_page
    @handled_pages_counter += 1

    #if @handled_pages_counter > 20
    #  raise ERROR
    #end
  end

  def cache fname, fcontent
    sanitized_name = fname.gsub '/', '_'
    cache_name = "#{@cache_dir}/#{sanitized_name}"
    IO.write(cache_name, fcontent)
  end

  def cache_page
    if @caching_class == WikimediaPageDoc
      cache @page_title, @page_text
    end
  end
end
