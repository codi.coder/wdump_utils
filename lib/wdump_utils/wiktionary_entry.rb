require 'set'

require_relative 'wiktionary_entry_section.rb'
require_relative 'wiktionary_entry_definition.rb'

class WiktionaryEntry
  def initialize(cache_dir:, word:)
    @sections = []

    filename = self.class.cache_filename(cache_dir: cache_dir, word: word)
    from_wiktionary_entr_doc_file(filename)
  end

  def sections
    @sections
  end

  def from_wiktionary_entr_doc_file(filename)
    #puts "FROM PAGE: #{filename}"
    definitions_wikitext = extract_definitions_wikitext(filename)
    definitions_plaintext = wikitext_to_plaintext(definitions_wikitext)
    from_definitions_plaintext(definitions_plaintext)
  end

  def extract_definitions_wikitext(filename)
    definitions_wikitext = ""

    current_section = nil
    was_section_appended = false

    IO.readlines(filename).each do |line|
      #puts "l: #{line}"

      if matches = line.match(/^===\s?{{S\|(.*?)\|.+?}}/)
        current_section = "=== #{matches[1]} ===\n"
        was_section_appended = false
      end

      if current_section
        if matches = line.match(/^#[^#](.*)$/)
          unless was_section_appended
            was_section_appended = true
            definitions_wikitext += "#{current_section.strip}\n"
          end

          definitions_wikitext += "#{matches[0].strip}\n"
        end
      end
    end

    definitions_wikitext
  end

  def wikitext_to_plaintext(wikitext)
    plaintext = wikitext

    # Remove formatting characters
    plaintext.gsub! /'''/, ''
    plaintext.gsub! /''/, ''

    # Keep "glossaire gramaticale"/context of definition information (vulgaire, botanique, etc)
    plaintext.gsub! /{{([^{}]+)\|fr}}/, '(\1)'

    # Remove citations
    3.times do
      plaintext.gsub! /{{[^{}]+}}/, ''
    end

    # Remove links to differently named pages ("[jour de la semaine|Jour de la semaine]" -> "Jour de la semaine")
    plaintext.gsub! /\[\[[^\]]*?\|([^\]]*)\]\]/, '\1'

    # Remove '[' and ']' characters
    plaintext.gsub! /[\[\]]/, ''

    plaintext
  end

  def from_definitions_plaintext(definitions_plaintext)
    definitions_plaintext.split("\n").each do |line|
      #puts line
      if line =~ /^=== (.+?) ===$/
        @sections << WiktionaryEntrySection.new($1.strip)
      elsif line =~ /^#([^*].*)/
        if section = @sections.last
          section.add_definition(WiktionaryEntryDefinition.new($1.strip))
        end
      elsif line =~ /^#\*(.*)/
        if definition = @sections.last.definitions.last
          definition.add_example($1.strip)
        end
      end
    end
  end

  def self.cache_filename(cache_dir:, word:)
    return nil if word.empty?

    filename = "#{cache_dir}/#{word}"
    #puts "#{word} = #{filename}"
    filename
  end

  def self.find_page(cache_dir:, word:)
    possibilities = []

    # Handle apostrophes
    if word =~ /..?['’’](.*)/
      word_after_apostrophe = $1
      possibilities |= [word_after_apostrophe.mb_chars.downcase.to_str]
      possibilities |= [word_after_apostrophe]
    end

    possibilities |= [word.mb_chars.downcase.to_str]
    possibilities |= [word]

    possibilities.each do |possibility|
      next unless filename = cache_filename(cache_dir: cache_dir, word: possibility)

      if File.exist?(filename)
        return possibility
      end
    end

    #puts "Found NOTHING for #{word}\t\ttried => #{possibilities.to_a}"
    nil
  end
end
