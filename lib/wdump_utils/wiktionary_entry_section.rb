class WiktionaryEntrySection
  def initialize(name)
    @name = name
    @definitions = []
  end

  def name
    @name
  end

  def add_definition(definition)
    @definitions << definition
  end

  def definitions
    @definitions
  end
end
