class WiktionaryEntryDefinition
  def initialize(body)
    @body = body
    @examples = []
  end

  def body
    @body
  end

  def examples
    @examples
  end

  def add_example(example)
    @examples << example
  end
end
