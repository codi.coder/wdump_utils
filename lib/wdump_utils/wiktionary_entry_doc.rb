require_relative 'wikimedia_page_doc'

class WiktionaryEntryDoc < WikimediaPageDoc
  @@language_dict = {
    'fr' => 'langue'
  }

  def initialize(cache_dir:, language:)
    super(cache_dir: cache_dir)
    @page_text_language = nil
    @language = language
    @caching_class = self.class
  end

  def handle_page
    super

    if @page_text_language = extract_language_only
      cache_entry
    end
  end

  def extract_language_only
    language_only = nil

    text_split = @page_text.split("\n").map do |line|
      line.strip
    end.join("\n")

    lang_header_regex = /^==\s?{{#{@@language_dict[@language]}\|#{@language}}}\s?==[^=](.+)/m
    if m = @page_text.match(lang_header_regex)
      after_lang_header = m[1]

      any_lang_header_regex = /(.+?)^==[^=]/m
      if m = after_lang_header.match(any_lang_header_regex)
        language_only = m[1]
      else
        language_only = after_lang_header
      end
    else
      #puts "#{@language} not found!?!"
    end

    language_only
  end

  def cache_entry
    if @caching_class == WiktionaryEntryDoc
      cache @page_title, @page_text_language
    end
  end
end
