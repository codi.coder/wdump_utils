Gem::Specification.new do |s|
  s.name = 'wdump_utils'
  s.version = '0.0.1'
  s.date = '2020-10-04'
  s.summary = ""
  s.description = ""
  s.authors = ["Pedro Aguiar"]
  s.email = 'paguiar@protonmail.com'
  s.files = Dir["{lib}/**/*.rb", "bin/*", "*.md"]
  s.executables = Dir["*.rb"]
  s.bindir = 'bin'
  s.require_path = 'lib'
  s.homepage = 'https://gitlab.com/paguiar/wdump_utils'
  s.license = ''
end
